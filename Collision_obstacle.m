function [ res ] = Collision_obstacle (path)
% return 1 if the path collides with obstacles, otherwise 0
global MAX_X MAX_Y MAP buffer_size
res = 0;
step = 1; % sampled points along trajectory
for i = 1: step: size(path,1)
    x = path(i,1);
    y = path(i,2);
    if (x < 1 || x > MAX_X || y < 1 || y > MAX_Y)
        res = 1; 
        return;
    end
    [row0, col0] = grid_cell(x,y);
    [row1, col1] = grid_cell(x-buffer_size,y-buffer_size);
    [row2, col2] = grid_cell(x-buffer_size,y+buffer_size);
    [row3, col3] = grid_cell(x+buffer_size,y-buffer_size);
     [row4, col4] = grid_cell(x+buffer_size,y+buffer_size);
    if (MAP(row0, col0) == -1 || MAP(row1,col1) == -1 || ...
            MAP(row2,col2) == -1 || MAP(row3,col3) == -1 || ...
            MAP(row4,col4) == -1) % note: x and y should be reversed in image
       res = 1; 
       return;
    end
end

end

