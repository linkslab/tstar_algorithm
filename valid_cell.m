function [ flag ] = valid_cell( i, j )
    global MAX_X MAX_Y
    flag = false;
    if i >= 1 && i <= MAX_X && j >= 1 && j <= MAX_Y
        flag = true;
    end
    return;
end