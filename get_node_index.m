function n_index = get_node_index(OPEN,xval,yval,thetaval)
    % find the index of a node in OPEN list
    i = 1;
    while(i <= length(OPEN) && (OPEN{i}{2}.x ~= xval || OPEN{i}{2}.y ~= yval || OPEN{i}{2}.theta ~= thetaval))
        i = i+1;
    end
    
    if i > 0 && i <= length(OPEN)
       n_index = i;
    else
       n_index = -1; % not in OPEN list
    end
end