% generate infeasible states (with heading ranges) against obstalces
function range = prune(current_x, current_y, neighbor_x, neighbor_y)
    global ANGLE_RES THETA;
    range = [];
    if Collision_obstacle([neighbor_x neighbor_y]) == 1
        ore = wrapTo2Pi(atan2(neighbor_y-current_y, neighbor_x-current_x));
        start_theta = floor(ore/ANGLE_RES);
        end_theta = ceil(ore/ANGLE_RES); 
        range = start_theta: end_theta;
        for m = 1: length(range)
            if range(m) < 0
               range(m) = range(m) + THETA+1; 
            elseif range(m) > THETA
               range(m) = range(m) - (THETA+1);
            end
        end
        range = unique(range);
    end

end

