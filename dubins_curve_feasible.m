% This function produces Dubins path
% This part of code is modified based on the works from:
% Andrew Walker and Ewing Kang

%%%%%%%%%%%%%%%%%%% Background Knowledge  %%%%%%%%%%%%%%%%%%%%%%
% there are 6 possible types of dubin's curve
% LSL = 1; LSR = 2; RSL = 3; RSR = 4; RLR = 5; LRL = 6;
% The three segment types a path can be made up of segments
% L_SEG = 1; S_SEG = 2; R_SEG = 3;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [path, time_cost] = dubins_curve_feasible(p1, p2, r, flag, stepsize)
    global speed
    % get dubins path parameters
    param = dubins_core_feasible(p1, p2, r, flag);
    
    if param.STATUS < 0 % no path
        path = 0;
        time_cost = -1;
    else
        path = dubins_path_sample_many(param, stepsize);
        if flag == 1
            time_cost = (param.SEG_param(1) + param.SEG_param(2) + param.SEG_param(3))/speed;
        else
            % heuristic uses maxspeed
            time_cost = (param.SEG_param(1) + param.SEG_param(2) + param.SEG_param(3))/1.0; 
        end
    end
end

function path = dubins_path_sample_many(param, stepsize)
    if param.STATUS < 0
        path = 0;
        return
    end
    length = dubins_length(param);
    path = -1 * ones(floor(length/stepsize), 3);
    x = 0;
    i = 1;
    while x <= length
        path(i, :) = dubins_path_sample(param, x);
        x = x + stepsize;
        i = i + 1;
    end
    return
end

function length = dubins_length(param)
    length = param.SEG_param(1);
    length = length + param.SEG_param(2);
    length = length + param.SEG_param(3); 
end

function [end_pt] = dubins_path_sample(param, t)
    if( t < 0 || t >= dubins_length(param) || param.STATUS < 0)
        end_pt = -1;
        return;
    end

    % tprime is the normalised variant of the parameter t
    tprime = t;

    % The translated initial configuration
    p_init = [0, 0, param.p_init(3)];
    
    % The three segment types a path can be made up of
    L_SEG = 1; S_SEG = 2; R_SEG = 3;

    % The segment types for each of the Path types
    DIRDATA = [ L_SEG, S_SEG, L_SEG ;...
                L_SEG, S_SEG, R_SEG ;...
                R_SEG, S_SEG, L_SEG ;...
                R_SEG, S_SEG, R_SEG ;...
                R_SEG, L_SEG, R_SEG ;...
                L_SEG, R_SEG, L_SEG ]; 

    % Generate the target configuration
    types = DIRDATA(param.type, :);
    param1 = param.SEG_param(1);
    param2 = param.SEG_param(2);
    mid_pt1 = dubins_segment( param1, p_init, types(1) , param.r);
    mid_pt2 = dubins_segment( param2, mid_pt1, types(2), param.r);
    
    % Actual calculation of the position of tprime within the curve
    if( tprime < param1 ) 
        end_pt = dubins_segment(tprime, p_init,  types(1), param.r);
    elseif( tprime < (param1+param2) ) 
        end_pt = dubins_segment( tprime-param1, mid_pt1, types(2), param.r); % if the second is also a turn, turn with the same radius
    else 
        end_pt = dubins_segment( tprime-param1-param2, mid_pt2,  types(3), param.r);
    end

    % scale the target configuration, translate back to the original starting point
    end_pt(1) = end_pt(1) + param.p_init(1);
    end_pt(2) = end_pt(2) + param.p_init(2);
    end_pt(3) = mod(end_pt(3), 2*pi);
    return;
end


function seg_end = dubins_segment(seg_param, seg_init, seg_type, r)
    L_SEG = 1;
    S_SEG = 2;
    R_SEG = 3;
    if( seg_type == L_SEG ) 
        seg_end(1) = seg_init(1) + r * sin(seg_init(3)+seg_param/r) - r * sin(seg_init(3));
        seg_end(2) = seg_init(2) - r * cos(seg_init(3)+seg_param/r) + r * cos(seg_init(3));
        seg_end(3) = seg_init(3) + seg_param/r;
    elseif( seg_type == R_SEG )
        seg_end(1) = seg_init(1) - r * sin(seg_init(3)-seg_param/r) + r * sin(seg_init(3));
        seg_end(2) = seg_init(2) + r * cos(seg_init(3)-seg_param/r) - r * cos(seg_init(3));
        seg_end(3) = seg_init(3) - seg_param/r;
    elseif( seg_type == S_SEG )
        seg_end(1) = seg_init(1) + cos(seg_init(3)) * seg_param;
        seg_end(2) = seg_init(2) + sin(seg_init(3)) * seg_param;
        seg_end(3) = seg_init(3);
    end
end