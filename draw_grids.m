function [ ] = draw_grids(separation)
% draw grids on top of current figure
    hold on;
    global MAX_X MAX_Y

    for k = 1:MAX_X
        x = [0.5 MAX_Y+0.5];
        y = [k*separation+0.5 k*separation+0.5];
        plot(x,y,'Color','w','LineStyle','-');
        plot(x,y,'Color','k','LineStyle',':');
    end

    for k = 1: MAX_Y
        x = [k*separation+0.5 k*separation+0.5];
        y = [0.5 MAX_X+0.5];
        plot(x,y,'Color','w','LineStyle','-');
        plot(x,y,'Color','k','LineStyle',':');
    end

end

