% generate list of successors based on the current input node
function successors = get_successors(node, cur_cost, node_Target)
    global MAX_X MAX_Y heuristic_flag step_flag eta riskweight ...
    sample_time radius THETA ANGLE_RES xTarget yTarget thetaTarget 
    
    successors={};
    successors_COUNT = 0;
   
    node_x = node.x;
    node_y = node.y;

    for k= 1:-1:-1
        for j= 1:-1:-1
            if ~(k == 0 && j == 0)
                s_x = node_x+k; % successor x
                s_y = node_y+j; % % successor y
                
                if((s_x >0 && s_x <= MAX_X) && (s_y >0 && s_y <= MAX_Y) && Collision_obstacle([s_x s_y]) == 0)         
                    %% from start to goal, get the orientation for space reduction
                    if s_x == xTarget && s_y == yTarget
                        range = thetaTarget; % otherwise path may not exist
                    else
                        % 8 orientation heading expansion, see paper for details
                        ore = wrapTo2Pi(atan2(yTarget - s_y, xTarget - s_x));

                        heuristic_range = [];
                        for m = 1: 2: THETA
                            if wrapTo2Pi(abs(ore - m*ANGLE_RES)) <= eta || wrapTo2Pi(abs(m*ANGLE_RES - ore)) <= eta
                                heuristic_range = [heuristic_range m];
                            end
                        end

                        infeasible_range = [];
                        for kk = -1: 1
                           for jj = -1: 1
                              if ~(kk == 0 && jj == 0) && Collision_obstacle([s_x+kk s_y+jj]) == 1
                                  tmp = prune(s_x,s_y,kk+s_x,jj+s_y); % infeasible configurations
                                  infeasible_range = [infeasible_range tmp];
                              end
                           end
                        end

                        range = setdiff([heuristic_range 0:2:THETA], sort(infeasible_range));
                        range = unique(range); % remove duplicates
                    end
                    
                    for theta = range
                            inClosedList = inClosed(s_x, s_y, theta);
                            if (inClosedList == 0) % if not in the CLOSED list
                                new_node = create_node(s_x, s_y, theta);
                                dubins_start = [node.x node.y node.theta*ANGLE_RES];
                                dubins_end = [new_node.x new_node.y new_node.theta*ANGLE_RES];
                                
                                [path, best_cost] = dubins_curve_feasible(dubins_start, dubins_end, ...
                                        radius, step_flag, sample_time);
                                
                                if best_cost == -1 % no path
                                    step_cost = Inf;
                                else
                                    if riskweight ~= 0 % associate with risk
                                        risk_tjc = get_risk(path);
                                        max_risk = (max(risk_tjc))^riskweight;
                                    elseif riskweight == 0
                                        max_risk = 1;
                                    end
                                    step_cost = best_cost * max_risk;
                                end
                                gn = cur_cost + step_cost; % update cumulative cost

                                %% heuristic cost
                                dubins_start = [new_node.x new_node.y new_node.theta*ANGLE_RES];
                                dubins_end = [node_Target.x node_Target.y node_Target.theta*ANGLE_RES];
                                [path, heuristic] = dubins_curve_feasible(dubins_start, dubins_end, ...
                                    radius, heuristic_flag, sample_time);
                                successors_COUNT = successors_COUNT + 1;
                                successors{successors_COUNT} = {new_node, gn, heuristic, gn + heuristic};
                            end
                        %end
                    end                    
                end% End of node within world bound
            end%End of if node is not its own successor 
        end%End of j 
    end%End of k  