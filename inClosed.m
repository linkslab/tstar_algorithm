% check if a node is in the CLOSED list
function res = inClosed(s_x, s_y, theta)
    global CLOSED;
    for k = 1: length(CLOSED)
        if(s_x == CLOSED{k}.x && s_y == CLOSED{k}.y && theta == CLOSED{k}.theta)
            res = 1; % in the CLOSED list already
            return;
        end;
    end
    res = 0;
end

