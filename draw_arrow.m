function [ ] = draw_arrow( x,y,theta)
    arrow_len = .4;

    if theta == 0
        vectarrow([x y], [x+arrow_len y],[0 .7 0],1);
    elseif theta == 1
        vectarrow([x y], [x+arrow_len y+arrow_len],[0 .7 0],1);
    elseif theta == 2
        vectarrow([x y], [x y+arrow_len],[0 .7 0],1);
    elseif theta == 3
        vectarrow([x y], [x-arrow_len y+arrow_len],[0 .7 0],1);
    elseif theta == 4    
        vectarrow([x y], [x-arrow_len y],[0 .7 0],1);
    elseif theta == 5
        vectarrow([x y], [x-arrow_len y-arrow_len],[0 .7 0],1);
    elseif theta == 6
        vectarrow([x y], [x y-arrow_len],[0 .7 0],1);
    elseif theta == 7
        vectarrow([x y], [x+arrow_len y-arrow_len],[0 .7 0],1);
    end
    return;
end

