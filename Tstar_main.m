%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                               The Tstar Algorithm[1]
% This is a MATLAB implementation of the following works using Dubins
% paths. Please consider citing it. 
% [1] J. Song, S. Gupta and T.A. Wettergren, "T*: Time-Optimal Risk-Aware Motion Planning 
% for Curvature-Constrained Vehicles". IEEE Robotics and Automation Letters, 4(1), 33-40.
% 
% Copyright: The LINKS lab, UConn, 2019.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear; 
close;
clc;

global MAX_X MAX_Y radius sample_time MAP heuristic_flag ...
    THETA ANGLE_RES buffer_size xTarget yTarget ...
    OPEN CLOSED thetaTarget eta speed step_flag riskweight tstar

%% Tstar parameters
riskweight = 0.5; % set to 0 for no risk considered
tstar = 6;
speed = 1; % vehicle motion
radius = 1;
eta = pi/2; % threshold for heading-based pruning
buffer_size = 0.05; % buffer around obstacles for safety
THETA = 7; % 8 heading options from 0 to 7 for each cell
ANGLE_RES = 2*pi/(THETA+1); % angle between two orientations
sample_time = 0.05;

step_flag = 1; % flag to consider collision in generating dubins curves
heuristic_flag = 0; % flag to not consider collision in generating dubins curves

%% load scenario
load scenario1.mat; % create your own grid-based scenario
xStart = 2; yStart = 13; thetaStart = 0; 
xTarget = 10; yTarget = 4; thetaTarget = 6;

% load scenario2.mat;
% xStart = 7; yStart = 13; thetaStart = 0; 
% xTarget = 8; yTarget = 7; thetaTarget = 7; 

imagesc(MAP');
set(gca,'YDir','normal')
colormap(gray); hold on;
plot(xStart,yStart,'mv','MarkerSize',10,'MarkerFaceColor','m');
plot(xTarget,yTarget,'mv','MarkerSize',10);
text(xStart,yStart-1,'Start', 'fontsize', 18);
text(xTarget,yTarget+1,'Target','fontsize', 18);
draw_grids(1);
set(gca,'xtick',[1:MAX_X])
set(gca,'ytick',[1:MAX_Y])
axis([.5 MAX_X+.5 .5 MAX_Y+.5])
hold on;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% OPEN list structure %%%%%%%
% | ON LIST 1/0 | node | parent | g(n) | h(n) | f(n)|
%% Node structure %%%%%%%%%%%% 
% | x, y, theta |
%% CLOSED list structure %%%%% 
% | X val | Y val |
%% Successor node format %%%%%
% | node | g(n) | h(n) | f(n) |
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% algorithm initialization
CLOSED = {};
CLOSED_COUNT = 0;
OPEN = {};
exp_array = {};
node = create_node(xStart, yStart, thetaStart);
node_Target = create_node(xTarget, yTarget, thetaTarget);
OPEN_COUNT = 1;
path_cost = 0;
dubins_start = [node.x node.y node.theta*ANGLE_RES];
dubins_end = [node_Target.x node_Target.y node_Target.theta*ANGLE_RES];
[path, goal_time] = dubins_curve_feasible(dubins_start, dubins_end, radius, heuristic_flag, sample_time);
new_cell = insert_open(node,node,path_cost,goal_time,goal_time); % insert the start point into OPEN 
new_cell{1} = 0;
OPEN{OPEN_COUNT} = new_cell;
CLOSED_COUNT = CLOSED_COUNT+1;
CLOSED{CLOSED_COUNT} = node;
NoPath=1;

%% algorithm main body
while((node.x ~= node_Target.x || node.y ~= node_Target.y || node.theta ~= node_Target.theta) && NoPath == 1) 
  
 % compute the costs of successor nodes and restore in exp_array
 disp('computing...');
 successors = get_successors(node, path_cost, node_Target);
 
 for i= 1: length(successors)  % update each successor node in OPEN/CLOSED list
    idx = get_node_index(OPEN, successors{i}{1}.x, successors{i}{1}.y, successors{i}{1}.theta);
    if idx ~= -1 % successor in the OPEN list
        if successors{i}{2} < OPEN{idx}{4}
            OPEN{idx}{3} = node;
            OPEN{idx}{4} = successors{i}{2}; % cumulative cost
            OPEN{idx}{5} = successors{i}{3}; % heuristic cost
            OPEN{idx}{6} = successors{i}{4}; % total cost
        end
    else
        % create a new node and add to OPEN
        OPEN_COUNT = OPEN_COUNT+1;
        newnode = successors{i}{1};
        OPEN{OPEN_COUNT} = insert_open(newnode, node, successors{i}{2}, successors{i}{3}, successors{i}{4});
    end
 end
 
  %% replace here with priority queue for quicker search
  index_min_node = min_fn(OPEN);
  
  % Move to list CLOSED
  if (index_min_node ~= -1) 
    node = OPEN{index_min_node}{2};
    path_cost = OPEN{index_min_node}{4};
    CLOSED_COUNT=CLOSED_COUNT+1; 
    CLOSED{CLOSED_COUNT} = node;
    OPEN{index_min_node}{1} = 0;
  else
    NoPath=0; % No path exists
  end
 
 plot(CLOSED{end}.x, CLOSED{end}.y, 'b*'); drawnow;  
end

%% Find the optimal path via backtracking
if (node.x == xTarget && node.y == yTarget && node.theta == thetaTarget)
   Optimal_path = [node.x, node.y, node.theta];
   idx = get_node_index(OPEN,node.x,node.y,node.theta);
   parent_x=OPEN{idx}{3}.x; 
   parent_y=OPEN{idx}{3}.y;
   parent_theta = OPEN{idx}{3}.theta;
   
   while(parent_x ~= xStart || parent_y ~= yStart || parent_theta ~= thetaStart)
        Optimal_path = [Optimal_path; parent_x parent_y parent_theta];
        idx = get_node_index(OPEN,parent_x,parent_y, parent_theta);
        parent_x = OPEN{idx}{3}.x;
        parent_y = OPEN{idx}{3}.y;
        parent_theta = OPEN{idx}{3}.theta;
   end
   Optimal_path = [Optimal_path; xStart, yStart, thetaStart]; % append start
     
    %% Plot the optimal path
    disp('Printing optimal path..');
    for k = 1: size(Optimal_path,1) % draw intermediate states
        x = Optimal_path(k,1);
        y = Optimal_path(k,2);
        theta = Optimal_path(k,3);
        draw_arrow(x,y,theta); hold on;
    end
    
    time_total = 0;
    for k = size(Optimal_path,1): -1: 2 % draw optimal path between states
        dubins_start = [Optimal_path(k,1), Optimal_path(k,2), Optimal_path(k,3)* ANGLE_RES];
        dubins_end = [Optimal_path(k-1,1), Optimal_path(k-1,2), Optimal_path(k-1,3)*ANGLE_RES];
        [path, path_cost] = dubins_curve_feasible(dubins_start, dubins_end, radius, step_flag, sample_time);
        time_total = time_total + path_cost;
        plot(path(:,1),path(:,2),'Color', [0 .5 0],'LineWidth',2); hold on;
    end
    disp('Done.');
else
    h = msgbox('No path found.','warn');
    uiwait(h, 10);
end

