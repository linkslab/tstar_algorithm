% compute collision distance for a pose
function d = get_collision_distance(x,y,theta) 
    global MAX_X MAX_Y MAP

    map = zeros(MAX_X, MAX_Y); % record whehter a cell has been considered

    % queue to store current frontier cells
    q = CQueue();
    node = create_node_discrete(round(x), round(y));
    q.push(node);

    d = Inf;

    while q.size() > 0 % go these rounds until the risk region is skipped
        for k = 1: q.size() % for each node, check its intersection and push its neighbors
            node = q.pop();

            % check if the cell is obstacle and intersects the collision line
            if MAP(node.i, node.j) == -1
                if theta ~= 0 && node.i-1/2 <= (node.j+1/2-y)/tan(theta) + x ...
                        && (node.j+1/2-y)/tan(theta) + x <= node.i+1/2
                   d_tmp = abs((node.j+1/2-y)/sin(theta));
                   d = min(d, d_tmp);
                end
                if theta ~= 0 && node.i-1/2 <= (node.j-1/2-y)/tan(theta) + x ...
                        && (node.j-1/2-y)/tan(theta) + x <= node.i+1/2
                   d_tmp = abs((node.j-1/2-y)/sin(theta));
                   d = min(d, d_tmp);
                end
                if theta ~= pi/2 && node.j-1/2 <= tan(theta)*(node.i-1/2-x)+y ...
                        && tan(theta)*(node.i-1/2-x)+y <= node.j+1/2
                   d_tmp = abs((node.i-1/2-x)/cos(theta));
                   d = min(d, d_tmp);
                end
                if theta ~= pi/2 && node.j-1/2 <= tan(theta)*(node.i+1/2-x)+y ...
                        && tan(theta)*(node.i+1/2-x)+y <= node.j+1/2
                   d_tmp = abs((node.i+1/2-x)/cos(theta));
                   d = min(d, d_tmp);
                end

                if d ~= Inf
                   return; 
                end
            end

            % otherwise, push its neighbors into the queue
            % note: expand the neighbors based on theta quadrant, map is to
            % record whether a cell has been added to the map
            neighbors = get_neighbors(node, x, y, theta, map); 
            for kk = 1: length(neighbors)
                node_tmp = neighbors(kk);
                map(node_tmp.i, node_tmp.j) = 1; % update map
                q.push(node_tmp); 
            end
        end
    end

end