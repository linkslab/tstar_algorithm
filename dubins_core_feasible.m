% This function computes feasible Dubins paths to be used for Tstar
% It is modified based on the works by:
% Andrew Walker and Ewing Kang

function param = dubins_core_feasible(p1, p2, r, flag)
    % flag = 1: to consider collision in step-wise cost
    % flag = 0: to not consider collision in heuristic
      
    global sample_time speed;
    % the return parameter
    param.p_init = p1;              % the initial configuration
    param.SEG_param = [0, 0, 0];    % the lengths of the three segments
    param.r = r;                    % model forward velocity / model angular velocity turning radius
    param.type = -1;                % path type. one of LSL, LSR, ... 
    param.STATUS = 0;

    % First, normalization of the problem
    dx = p2(1) - p1(1);
    dy = p2(2) - p1(2);
    D = sqrt(dx^2 + dy^2);
    d = D; % distance is shrunk by r, this makes lengh calculation easy
    if( r <= 0 || r <= 0)
        param.STATUS = -1;
        return;
    end
    theta = mod(atan2(dy, dx), 2*pi);
    alpha = mod((p1(3) - theta), 2*pi);
    beta  = mod((p2(3) - theta), 2*pi);

    % Second, we find all possible Dubins curves
    best_word = -1;
    best_cost = -1;
    test_param(1,:) = dubins_LSL(alpha, beta, d, r);
    test_param(2,:) = dubins_LSR(alpha, beta, d, r);
    test_param(3,:) = dubins_RSL(alpha, beta, d, r);
    test_param(4,:) = dubins_RSR(alpha, beta, d, r);
    test_param(5,:) = dubins_RLR(alpha, beta, d, r);
    test_param(6,:) = dubins_LRL(alpha, beta, d, r);
    
    for i = 1:6
        if(test_param(i,1) ~= -1 && test_param(i,2) ~= -1 && test_param(i,3) ~= -1) % existance
            % the dubins path should be feasible
            tmp_param = param;
            tmp_param.SEG_param = test_param(i,:);
            tmp_param.type = i; 
            path = dubins_path_sample_many(tmp_param, sample_time);
            if isequal(path, 0) == 0 % feasible path
                if flag == 1 % consider feasible path in step-wise cost
                    if Collision_obstacle(path) == 0
                        cost = sum(test_param(i,:))/speed;
                        if ((cost < best_cost) || (best_cost == -1))
                            best_word = i;
                            best_cost = cost;
                            param.SEG_param = test_param(i,:);
                            param.type = i;
                        end  
                    end
                else % do not consider collision in heuristic cost
                    cost = sum(test_param(i,:));
                    if ((cost < best_cost) || (best_cost == -1))
                        best_word = i;
                        best_cost = cost;
                        param.SEG_param = test_param(i,:);
                        param.type = i;
                    end
                end
            end
        end
    end

    if(best_word == -1) 
        param.STATUS = -2; % no path
        return;
    else
        return;
    end
end

function param = dubins_LSL(alpha, beta, d, r)
    p_squared = (d*d) + 2*r*r*(1-cos(alpha - beta)) + 2*d*(r*sin(alpha) - r*sin(beta));
    if( p_squared < 0 )
        param = [-1 -1 -1];
        return;
    else
        p = sqrt( p_squared );
        num1 = d + r*sin(alpha) - r*sin(beta);
        num2 = r*cos(beta)-r*cos(alpha);
        tmp1 = atan2( (r-r)*num1 + p*num2, p*num1 - (r-r)*num2);
        t = mod((-alpha + tmp1), 2*pi);
        q = mod((beta - tmp1), 2*pi);
        param(1) = t*r;
        param(2) = p;
        param(3) = q*r;
        return ;
    end
end

function param = dubins_LSR(alpha, beta, d, r)
    p_squared = (d*d) + 2*r*r*(cos(alpha - beta)-1) + 2*d*(r*sin(alpha) + r*sin(beta));
    if( p_squared < 0 )
        param = [-1 -1 -1];
        return;
    else
        p = sqrt( p_squared );
        num1 = d + r*sin(alpha) + r*sin(beta);
        num2 = r*cos(beta)+r*cos(alpha);
        tmp1 = atan2(2*r*num1 - p*num2, p*num1 + 2*r*num2);
        t = mod((-alpha + tmp1), 2*pi);
        q = mod((-beta + tmp1), 2*pi);
        param(1) = t*r;
        param(2) = p;
        param(3) = q*r;
        return ;
    end   

end
function param = dubins_RSL(alpha, beta, d, r)
    p_squared = (d*d) + 2*r*r*(cos(alpha - beta)-1) - 2*d*(r*sin(alpha) + r*sin(beta));
    if( p_squared < 0 )
        param = [-1 -1 -1];
        return;
    else
        p = sqrt( p_squared );
        num1 = d - r*sin(alpha) - r*sin(beta);
        num2 = r*cos(beta)+r*cos(alpha);
        tmp1 = atan2(-2*r*num1 + p*num2, p*num1 + 2*r*num2);
        t = mod(( alpha - tmp1 ), 2*pi);
        q = mod((beta - tmp1), 2*pi);
        param(1) = t*r;
        param(2) = p;
        param(3) = q*r;
        return ;
    end
end

function param = dubins_RSR(alpha, beta, d, r)
p_squared = (d*d) + 2*r*r*(1-cos(alpha - beta)) + 2*d*(-r*sin(alpha) + r*sin(beta));
    if( p_squared < 0 )
        param = [-1 -1 -1];
        return;
    else
        p = sqrt( p_squared );
        num1 = d - r*sin(alpha) + r*sin(beta);
        num2 = -r*cos(beta)+r*cos(alpha);
        tmp1 = atan2( -(r-r)*num1 + p*num2, p*num1 + (r-r)*num2);
        t = mod(( alpha - tmp1 ), 2*pi);
        q = mod(( -beta + tmp1 ), 2*pi);
        param(1) = t*r;
        param(2) = p;
        param(3) = q*r;
        return ;
    end
end

function param = dubins_RLR(alpha, beta, d, r)
    if (r ~= r)
        param = [-1 -1 -1]; 
        return;
    else
        d = d/r;
        tmp_rlr = (6. - d*d + 2*cos(alpha - beta) + 2*d*(sin(alpha)-sin(beta))) / 8.;
        if(abs(tmp_rlr) > 1)
        param = [-1 -1 -1]; 
        return;
    else
        p = mod(( 2*pi - acos( tmp_rlr ) ), 2*pi);
        t = mod((alpha - atan2( cos(alpha)-cos(beta), d-sin(alpha)+sin(beta) ) + mod(p/2, 2*pi)), 2*pi);
        q = mod((alpha - beta - t + mod(p, 2*pi)), 2*pi);
        param(1) = t * r;
        param(2) = p * r;
        param(3) = q * r;
        return;
        end  
    end
end

function param = dubins_LRL(alpha, beta, d, r)
    if (r ~= r)
        param = [-1 -1 -1]; return;
    else
        d = d/r;
        tmp_lrl = (6. - d*d + 2*cos(alpha - beta) + 2*d*(- sin(alpha) + sin(beta))) / 8.;
        if(abs(tmp_lrl) > 1)
        param = [-1 -1 -1]; return;
        else
            p = mod(( 2*pi - acos( tmp_lrl ) ), 2*pi);
            t = mod((-alpha - atan2( cos(alpha)-cos(beta), d+sin(alpha)-sin(beta) ) + p/2), 2*pi);
            q = mod((mod(beta, 2*pi) - alpha -t + mod(p, 2*pi)), 2*pi);
            param(1) = t * r;
            param(2) = p * r;
            param(3) = q * r;
            return;
        end
    end
end


function path = dubins_path_sample_many(param, stepsize)
    if param.STATUS < 0
        path = 0;
        return
    end
    length = dubins_length(param);
    path = -1 * ones(floor(length/stepsize), 3);
    x = 0;
    i = 1;
    while x <= length
        path(i, :) = dubins_path_sample( param, x );
        x = x + stepsize;
        i = i + 1;
    end
    return
end

function length = dubins_length(param)
    length = param.SEG_param(1) + param.SEG_param(2) + param.SEG_param(3);
end


function [end_pt] = dubins_path_sample(param, t)
    if( t < 0 || t >= dubins_length(param) || param.STATUS < 0)
        end_pt = -1;
        return;
    end

    % tprime is the normalised variant of the parameter t
    tprime = t;
    p_init = [0, 0, param.p_init(3)];
    
    % The three segment types a path can be made up of
    L_SEG = 1;
    S_SEG = 2;
    R_SEG = 3;

    % The segment types for each of the Path types
    DIRDATA = [ L_SEG, S_SEG, L_SEG ;...
                L_SEG, S_SEG, R_SEG ;...
                R_SEG, S_SEG, L_SEG ;...
                R_SEG, S_SEG, R_SEG ;...
                R_SEG, L_SEG, R_SEG ;...
                L_SEG, R_SEG, L_SEG ]; 

    % Generate the target configuration
    types = DIRDATA(param.type, :);
    param1 = param.SEG_param(1);
    param2 = param.SEG_param(2);
    mid_pt1 = dubins_segment( param1, p_init, types(1) , param.r);
    mid_pt2 = dubins_segment( param2, mid_pt1, types(2), param.r);
    
    % Actual calculation of the position of tprime within the curve
    if( tprime < param1 ) 
        end_pt = dubins_segment(tprime, p_init,  types(1), param.r);
    elseif( tprime < (param1+param2) ) 
        end_pt = dubins_segment( tprime-param1, mid_pt1,  types(2), param.r); % if the second is also a turn, turn with the same radius
    else 
        end_pt = dubins_segment( tprime-param1-param2, mid_pt2,  types(3), param.r);
    end

    % scale the target configuration, translate back to the original starting point
    end_pt(1) = end_pt(1) + param.p_init(1);
    end_pt(2) = end_pt(2) + param.p_init(2);
    end_pt(3) = mod(end_pt(3), 2*pi);
    return;
end

function seg_end = dubins_segment(seg_param, seg_init, seg_type, r)
    L_SEG = 1;
    S_SEG = 2;
    R_SEG = 3;
    if( seg_type == L_SEG ) 
        seg_end(1) = seg_init(1) + r * sin(seg_init(3)+seg_param/r) - r * sin(seg_init(3));
        seg_end(2) = seg_init(2) - r * cos(seg_init(3)+seg_param/r) + r * cos(seg_init(3));
        seg_end(3) = seg_init(3) + seg_param/r;
    elseif( seg_type == R_SEG )
        seg_end(1) = seg_init(1) - r * sin(seg_init(3)-seg_param/r) + r * sin(seg_init(3));
        seg_end(2) = seg_init(2) + r * cos(seg_init(3)-seg_param/r) - r * cos(seg_init(3));
        seg_end(3) = seg_init(3) - seg_param/r;
    elseif( seg_type == S_SEG )
        seg_end(1) = seg_init(1) + cos(seg_init(3)) * seg_param;
        seg_end(2) = seg_init(2) + sin(seg_init(3)) * seg_param;
        seg_end(3) = seg_init(3);
    end
end