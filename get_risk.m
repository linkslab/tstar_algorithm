% calculate risk along a planned path
function risk_tjc = get_risk(paths) 
    global tstar speed
    risk_tjc = [];
    step = 8; % down sampled points along trajectory
    for i = 2: step: size(paths, 1)
        x = paths(i,1);
        y = paths(i,2);
        theta = paths(i,3);
        
        % collision time
        t = get_collision_distance(x,y,theta)/speed;
        
        risk_pose = 1; % default: collision free
        if t < tstar
            risk_pose = 1 + log(tstar/t);
        end
        risk_tjc = [risk_tjc; risk_pose];
    end
        return;
end
