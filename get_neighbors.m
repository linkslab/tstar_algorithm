% expand cells in the map based on current considered node and the 
% quadrant determined by theta
% a valid neighbor cell must be within radius to the sample pose
% map: records which cell has been added to neighbors already
% 0: not considered yet; 1: already added to the queue

function neighbors = get_neighbors(node, x, y, theta, map) 
global tstar speed

dist = tstar*speed;

neighbors = [];

if theta == 0 % search along x-axis
    if valid_cell(node.i+1, node.j) && map(node.i+1, node.j) == 0 && ...
            min([(node.i+1/2-x)^2+(node.j+1/2-y)^2 (node.i+1/2-x)^2+(node.j-1/2-y)^2 (node.i+3/2-x)^2+(node.j+1/2-y)^2 (node.i+3/2-x)^2+(node.j-1/2-y)^2]) <= dist^2
        neighbors = create_node_discrete(node.i+1, node.j);
    end
elseif theta > 0 && theta < pi/2 % 1st quadrant
    if valid_cell(node.i, node.j+1) && map(node.i, node.j+1) == 0 && ...
            min([(node.i-1/2-x)^2+(node.j+1/2-y)^2  (node.i+1/2-x)^2+(node.j+1/2-y)^2 (node.i-1/2-x)^2+(node.j+3/2-y)^2 (node.i+1/2-x)^2+(node.j+3/2-y)^2]) <= dist^2 % neareast corner point
       neighbors = [neighbors create_node_discrete(node.i, node.j+1)];
    end
    
    if valid_cell(node.i+1, node.j) && map(node.i+1, node.j) == 0 && ...
            min([(node.i+1/2-x)^2+(node.j+1/2-y)^2 (node.i+1/2-x)^2+(node.j-1/2-y)^2 (node.i+3/2-x)^2+(node.j+1/2-y)^2 (node.i+3/2-x)^2+(node.j-1/2-y)^2]) <= dist^2
       neighbors = [neighbors create_node_discrete(node.i+1, node.j)];
    end   
elseif theta == pi/2 % search along y-axis
    if valid_cell(node.i, node.j+1) && map(node.i, node.j+1) == 0 && ...
            min([(node.i-1/2-x)^2+(node.j+1/2-y)^2  (node.i+1/2-x)^2+(node.j+1/2-y)^2 (node.i-1/2-x)^2+(node.j+3/2-y)^2 (node.i+1/2-x)^2+(node.j+3/2-y)^2]) <= dist^2
       neighbors = [neighbors create_node_discrete(node.i, node.j+1)]; 
    end
elseif theta > pi/2 && theta < pi %% 2nd quadrant
    if valid_cell(node.i, node.j+1) && map(node.i, node.j+1) == 0 && ...
            min([(node.i-1/2-x)^2+(node.j+1/2-y)^2  (node.i+1/2-x)^2+(node.j+1/2-y)^2 (node.i-1/2-x)^2+(node.j+3/2-y)^2 (node.i+1/2-x)^2+(node.j+3/2-y)^2]) <= dist^2
       neighbors = [neighbors create_node_discrete(node.i, node.j+1)];
    end
    
    if valid_cell(node.i-1, node.j) && map(node.i-1, node.j) == 0 && ...
            min([(node.i-1/2-x)^2+(node.j-1/2-y)^2 (node.i-1/2-x)^2+(node.j+1/2-y)^2 (node.i-3/2-x)^2+(node.j-1/2-y)^2 (node.i-3/2-x)^2+(node.j+1/2-y)^2]) <= dist^2
       neighbors = [neighbors create_node_discrete(node.i-1, node.j)];
    end
elseif theta == pi % along negative x-axis
    if valid_cell(node.i-1, node.j) && map(node.i-1, node.j) == 0 && ...
            min([(node.i-1/2-x)^2+(node.j-1/2-y)^2 (node.i-1/2-x)^2+(node.j+1/2-y)^2 (node.i-3/2-x)^2+(node.j-1/2-y)^2 (node.i-3/2-x)^2+(node.j+1/2-y)^2]) <= dist^2
       neighbors = [neighbors create_node_discrete(node.i-1, node.j)]; 
    end
elseif theta > pi && theta < pi/2*3 % 3rd quarant
    if valid_cell(node.i-1, node.j) && map(node.i-1, node.j) == 0 && ...
            min([(node.i-1/2-x)^2+(node.j-1/2-y)^2 (node.i-1/2-x)^2+(node.j+1/2-y)^2 (node.i-3/2-x)^2+(node.j-1/2-y)^2 (node.i-3/2-x)^2+(node.j+1/2-y)^2]) <= dist^2
       neighbors = [neighbors create_node_discrete(node.i-1, node.j)]; 
    end
    
    if valid_cell(node.i, node.j-1) && map(node.i, node.j-1) == 0 && ...
            min([(node.i-1/2-x)^2+(node.j-1/2-y)^2 (node.i+1/2-x)^2+(node.j-1/2-y)^2 (node.i-1/2-x)^2+(node.j-3/2-y)^2 (node.i+1/2-x)^2+(node.j-3/2-y)^2]) <= dist^2
       neighbors = [neighbors create_node_discrete(node.i, node.j-1)];
    end
elseif theta == pi/2*3 % along negative y-axis
    if valid_cell(node.i, node.j-1) && map(node.i, node.j-1) == 0 && ...
            min([(node.i-1/2-x)^2+(node.j-1/2-y)^2 (node.i+1/2-x)^2+(node.j-1/2-y)^2 (node.i-1/2-x)^2+(node.j-3/2-y)^2 (node.i+1/2-x)^2+(node.j-3/2-y)^2]) <= dist^2
       neighbors = [neighbors create_node_discrete(node.i, node.j-1)]; 
    end
elseif theta > pi/2*3 && theta < 2*pi % 4th quarant
    if valid_cell(node.i, node.j-1) && map(node.i, node.j-1) == 0 && ...
            min([(node.i-1/2-x)^2+(node.j-1/2-y)^2 (node.i+1/2-x)^2+(node.j-1/2-y)^2 (node.i-1/2-x)^2+(node.j-3/2-y)^2 (node.i+1/2-x)^2+(node.j-3/2-y)^2]) <= dist^2
       neighbors = [neighbors create_node_discrete(node.i, node.j-1)]; 
    end
    
    if valid_cell(node.i+1, node.j) && map(node.i+1, node.j) == 0 && ...
            min([(node.i+1/2-x)^2+(node.j+1/2-y)^2 (node.i+1/2-x)^2+(node.j-1/2-y)^2 (node.i+3/2-x)^2+(node.j+1/2-y)^2 (node.i+3/2-x)^2+(node.j-1/2-y)^2]) <= dist^2
       neighbors = [neighbors create_node_discrete(node.i+1, node.j)];
    end
end
return;
end