function output_row = insert_open(newnode, parent_node,hn,gn,fn)
% generate a new row in the format of OPEN list
output_row = {1 newnode parent_node hn gn fn};
end