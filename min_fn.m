% return the node with minimum fn in OPEN list
function i_min = min_fn(OPEN)
    cur_min = Inf;
    i_min = -1;

    for j = 1: length(OPEN)
        % not assigned yet or has a smaller cost
        if OPEN{j}{1} == 1 && (i_min == -1 || OPEN{j}{6} < cur_min)
            cur_min = OPEN{j}{6};
            i_min = j;
        end
    end
end