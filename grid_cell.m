function [row, col ] = grid_cell( x,y )
% determine the cell location according to absolute location x and y

row = round(x);
col = round(y);
return;

end

