# Tstar_algorithm
Description: 

This is a MATLAB implementation of the following works using Dubins paths. Please cite if you use. 
J. Song, S. Gupta and T.A. Wettergren, "T*: Time-Optimal Risk-Aware Motion Planning 
for Curvature-Constrained Vehicles". IEEE Robotics and Automation Letters, 4(1), 33-40.

Copyright: The LINKS lab, UConn, 2019.

Usage: run Tstar_main.m to generate time-optimal Dubins path between two states in the presence of obstacles



